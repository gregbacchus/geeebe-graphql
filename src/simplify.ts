import {
  ArgumentNode,
  DirectiveNode,
  FieldNode,
  FragmentDefinitionNode,
  FragmentSpreadNode,
  SelectionNode,
  ValueNode,
  VariableDefinitionNode,
} from 'graphql';
import {
  IArgument,
  IDirective,
  IField,
  IFragmentDefinition,
  IFragmentMap,
  IFragmentSpread,
  IInfo,
  IVariableDefinition,
  Selection,
} from './types';

export function simplifyInfo(info: any): IInfo {
  return {
    fieldName: info.fieldName,
    fieldNodes: info.fieldNodes.map(simplifyField),
    fragments: simplifyFragmentMap(info.fragments),
    // operation: IOperation,
    // parentType: any,
    // path: IPath,
    // returnType: IReturnType,
    // rootValue: any,
    schema: info.schema,
    // variableValues: IVariableValueMap,
  };
}

export function simplifyFragmentMap(fragments: any): IFragmentMap {
  const result: IFragmentMap = {};
  for (const key of Object.keys(fragments)) {
    result[key] = simplifyFragmentDefinition(fragments[key]);
  }
  return result;
}

export function simplifyFragmentDefinition(fragment: FragmentDefinitionNode): IFragmentDefinition {
  return {
    directives: fragment.directives ? fragment.directives.map(simplifyDirective) : [],
    name: fragment.name.value,
    selections: fragment.selectionSet ? fragment.selectionSet.selections.map(simplifySelection) : [],
    type: fragment.typeCondition.name.value,
    variableDefinitions: fragment.variableDefinitions ? fragment.variableDefinitions.map(simplifyVariableDefinitions) : [],
  };
}

export function simplifyVariableDefinitions(_variable: VariableDefinitionNode): IVariableDefinition {
  return {
    // TODO
  };
}

export function simplifyField(field: FieldNode): IField {
  return {
    alias: field.alias ? field.alias.value : undefined,
    arguments: field.arguments ? field.arguments.map(simplifyArgument) : [],
    directives: field.directives ? field.directives.map(simplifyDirective) : [],
    kind: 'Field',
    name: field.name.value,
    selections: field.selectionSet ? field.selectionSet.selections.map(simplifySelection) : [],
  };
}

export function simplifyArgument(argument: ArgumentNode): IArgument {
  return {
    name: argument.name.value,
    value: simplifyValue(argument.value),
  };
}

export function simplifyValue(value: ValueNode): any {
  switch (value.kind) {
    case 'BooleanValue':
      return value.value;
    case 'EnumValue':
      return value.value;
    case 'FloatValue':
      return value.value;
    case 'IntValue':
      return value.value;
    case 'ListValue':
      return value.values.map(simplifyValue);
    case 'NullValue':
      return null;
    case 'ObjectValue': {
      const result: any = {};
      value.fields.forEach((field) => {
        result[field.name.value] = simplifyValue(field.value);
      });
      return result;
    }
    case 'StringValue':
      return value.value;
    case 'Variable':
    // TODO return value.value;
  }
}

export function simplifyDirective(directive: DirectiveNode): IDirective {
  return {
    arguments: directive.arguments ? directive.arguments.map(simplifyArgument) : [],
    name: directive.name.value,
  };
}

export function simplifySelection(selection: SelectionNode): Selection {
  switch (selection.kind) {
    case 'Field':
      return simplifyField(selection);
    case 'FragmentSpread':
      return simplifyFragmentSpread(selection);
    // TODO InlineFragmentNode
  }
}

export function simplifyFragmentSpread(fragmentSpread: FragmentSpreadNode): IFragmentSpread {
  return {
    directives: fragmentSpread.directives ? fragmentSpread.directives.map(simplifyDirective) : [],
    kind: 'FragmentSpread',
    name: fragmentSpread.name.value,
  };
}
