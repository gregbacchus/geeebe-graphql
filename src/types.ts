import { GraphQLSchema } from 'graphql';

export interface IInfo {
  fieldName: string;
  fieldNodes: IField[];
  fragments: IFragmentMap;
  // operation: IOperation;
  // parentType: any;
  // path: IPath;
  // returnType: IReturnType;
  // rootValue: any;
  schema: GraphQLSchema;
  // variableValues: IVariableValueMap;
}

export interface IArgument {
  name: string;
  value: any;
}

export type Selection = IField | IFragmentSpread | undefined;

export interface IField {
  alias?: string;
  arguments: IArgument[];
  directives: IDirective[];
  kind: 'Field';
  name: string;
  selections: Selection[];
}

export interface IDirective {
  arguments: IArgument[];
  name: string;
}

export interface IFragmentSpread {
  directives: IDirective[];
  kind: 'FragmentSpread';
  name: string;
}

export interface IFragmentMap { [key: string]: IFragmentDefinition; }

export interface IFragmentDefinition {
  directives: IDirective[];
  name: string;
  selections: Selection[];
  type: string;
  variableDefinitions: IVariableDefinition[];
}

export interface IOperation {
  directives: IDirective[];
  name: string;
  operation: string;
  selections: Selection[];
  variableDefinition: IVariableDefinition[];
}

export interface IVariableDefinition {
}

export interface IPath {
}

export interface IVariableDefinition {
}

export interface IReturnType {
}

export interface IVariableValueMap { [key: string]: IVariableValue; }

export interface IVariableValue {
}
