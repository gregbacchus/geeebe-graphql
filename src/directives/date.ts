import { defaultFieldResolver, GraphQLField, GraphQLString } from 'graphql';
import { SchemaDirectiveVisitor } from 'graphql-tools';
import * as moment from 'moment';
import 'moment-timezone';
import { simplifyInfo } from '../simplify';
import { IDirective } from '../types';

/**
 * Using momemtjs format
 */
export class DateFormatDirective extends SchemaDirectiveVisitor {
  public visitFieldDefinition(field: GraphQLField<any, any>) {
    const { resolve = defaultFieldResolver } = field;
    const { format } = this.args;
    field.resolve = async (obj: any, args: any, context: any, infoRaw: any) => {
      const date = moment(await resolve.apply(this, [obj, args, context, infoRaw]));

      const info = simplifyInfo(infoRaw);
      const directives = info.fieldNodes[0].directives;
      const directive = directives
        ? directives.find((directive) => directive.name === 'date')
        : undefined;
      if (!directive) return date.toISOString();
      const requestedFormat = getArgumentValue(directive, 'format');
      const tz = getArgumentValue(directive, 'tz');
      const formatted = date.tz(tz)
        .format(requestedFormat || format);
      return formatted;
    };
    // The formatted Date becomes a String, so the field type must change:
    field.type = GraphQLString;
  }
}

function getArgumentValue(directive: IDirective, name: string) {
  if (!directive) return undefined;
  const argument = directive.arguments.find((argument) => argument.name === name);
  return argument ? argument.value : undefined;
}
